<?php
class Controller_Api extends Controller_Rest
{
    public function post_vote() {
        // ログインされているか
        if (Auth::check()) {
            // 投票数に+1する
            $id = Input::post('id');
            $image = Model_Image::find($id);
            $image->votes = $image->votes + 1;
            $image->save();
        }
        
        return array('message' => '処理終了');
    }
}