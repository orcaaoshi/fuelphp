<?php

class Controller_Vote extends Controller {
    public function action_login() {
        if (Input::post('id') != '') {  // username を id に修正
            if (Auth::login(Input::post('id'), Input::post('password'))) {
                Response::redirect('vote/view');  // ログインに成功した
            }
        }
        
        return Response::forge(View::forge('vote/login'));
    }
    public function action_logout() {
        Auth::logout();
        Response::redirect('vote/login');
    }
    public function action_view() {
        if (! Auth::check()) {  // ログインできていない場合
            Response::redirect('vote/login');
        }
        
        // データをロード
        $images = Model_Image::find('all');
        $data = array('images' => $images);
        return Response::forge(View::forge('vote/view', $data));
    }
}
