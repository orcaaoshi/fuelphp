<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ログイン</title>
	<style>
		body { margin: 40px; }
	</style>
</head>
<body>
  <?php echo Form::open('vote/login'); ?>
    ログイン<br>
    ユーザーID:
    <?php echo Form::input('id', ''); ?>
    <br>
    パスワード:
    <?php echo Form::password('password', ''); ?>
    <br>
    <?php echo Form::submit('login', 'ログイン'); ?>
  <?php echo Form::close(); ?>
</body>
</html>
